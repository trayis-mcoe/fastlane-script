describe Fastlane::Actions::LbActionsAction do
  describe '#run' do
    it 'prints a message' do
      expect(Fastlane::UI).to receive(:message).with("The lb_actions plugin is working!")

      Fastlane::Actions::LbActionsAction.run(nil)
    end
  end
end
