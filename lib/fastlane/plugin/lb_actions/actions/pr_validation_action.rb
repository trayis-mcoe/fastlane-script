require 'fastlane/action'
require_relative '../helper/lb_actions_helper'

module Fastlane
  module Actions
    class PrValidationAction < Action
      def self.run(params)
        if ENV['build_url']
          other_action.notify_to_teams(message: "🚀 Started PR Validation for <a href='#{ENV['build_url'].to_s}'>build #{ENV['buildnumber'].to_s}</a>.")
        else
          other_action.notify_to_teams(message: "🚀 Started fastlane for PR Validation.")
        end
        
      end

      def self.description
        "This actions performs validation of given pr given project. It is responsible for running Lint, running unit tests."
      end

      def self.details
        "You can use this action to do cool things..."
      end

      def self.available_options
      end

      def self.output
        [
          ['PR_VALIDATION_CUSTOM_VALUE', 'A description of what this value contains']
        ]
      end

      def self.return_value
      end

      def self.authors
        ["mukundrd"]
      end

      def self.is_supported?(platform)
        platform == :android
      end
    end
  end
end
  