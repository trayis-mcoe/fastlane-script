require 'fastlane/action'
require_relative '../helper/lb_actions_helper'

module Fastlane
  module Actions
    class LbActionsAction < Action
      def self.run(params)
        UI.message("The lb_actions plugin is working!")
      end

      def self.description
        "This is a collections of actions for LBrands CI/CD scripting. Come back to see more details!!"
      end

      def self.authors
        ["Mukund Desai"]
      end

      def self.return_value
        # If your method provides a return value, you can describe here what it does
      end

      def self.details
        # Optional:
        "This is a collections of actions for LBrands CI/CD scripting. Come back to see more details!!"
      end

      def self.available_options
        [
          # FastlaneCore::ConfigItem.new(key: :your_option,
          #                         env_name: "LB_ACTIONS_YOUR_OPTION",
          #                      description: "A description of your option",
          #                         optional: false,
          #                             type: String)
        ]
      end

      def self.is_supported?(platform)
        # Adjust this if your plugin only works for a particular platform (iOS vs. Android, for example)
        # See: https://docs.fastlane.tools/advanced/#control-configuration-by-lane-and-by-platform
        #
        # [:ios, :mac, :android].include?(platform)
        true
      end
    end
  end
end
