require 'fastlane/action'
require_relative '../helper/lb_actions_helper'

$platform="android"

$package_name="com.lbrands.fastlanescripts"
$app_name="Android Fastlane Script"

$teams_url="https://outlook.office.com/webhook/dca6a5a1-de84-49dd-bca7-b90b001126f2@865f3fef-b96b-4b1d-a7b6-0b4b3fcfc87e/IncomingWebhook/cd8f0c311a0c44d5a88ef50c17a10cc8/ae013d99-9239-4212-86b8-70d8d30ad6e7"
$app_icon_url="https://hkappschannel.com/wp-content/uploads/2014/06/android1-90x90.png"

$build_lane="Not Defined"

module Fastlane
  module Actions
    class NotifyToTeamsAction < Action
      def self.run(params)
        message = params[:message]
        reason = ""
        if (params[:reason] != nil)
          reason = "<tr><td colspan=2>#{params[:reason]}</td></tr>"
        end

        begin
          other_action.teams(
            title: $app_name,
            facts:[
              {
                "name"=>"Platform",
                "value"=>$platform
              },
              {
                "name"=>"Lane",
                "value"=>$build_lane
              }
            ],
            message: "<table><tr><td style='vertical-align: top; width: 50px'><img src='#{$app_icon_url}' width='50' height='50'></td><td>#{message}</td></tr>#{reason}</table>",
            teams_url: $teams_url
          )
        rescue => ex
          UI.error ex
        end
      end

      def self.description
        "This is a collections of actions for LBrands CI/CD scripting. Come back to see more details!!"
      end

      def self.authors
        ["Mukund Desai"]
      end

      def self.return_value
        # If your method provides a return value, you can describe here what it does
      end

      def self.details
        # Optional:
        "This is a collections of actions for LBrands CI/CD scripting. Come back to see more details!!"
      end

      def self.available_options
        [
          FastlaneCore::ConfigItem.new(key: :message,
                                  env_name: "LB_ACTIONS_MESSAGE",
                               description: "Message to be sent to teams",
                                  optional: false,
                                      type: String),
          FastlaneCore::ConfigItem.new(key: :reason,
                                  env_name: "LB_ACTIONS_REASON",
                                description: "Reason to be print along message",
                                  optional: true,
                                      type: String)
        ]
      end

      def self.is_supported?(platform)
        # Adjust this if your plugin only works for a particular platform (iOS vs. Android, for example)
        # See: https://docs.fastlane.tools/advanced/#control-configuration-by-lane-and-by-platform
        #
        # [:ios, :mac, :android].include?(platform)
        true
      end
    end
  end
end
